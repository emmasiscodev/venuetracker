//
//  InteractorTests.swift
//  VenueTrackerDemoTests
//
//  Created by Emma Sisco on 2/17/21.
//

import Foundation
import XCTest

class InteractorTests: XCTestCase {
    
    func testExtractResponse() throws {
        let testInteractor = VenueDetailsInteractor()
        let serverResponse = TestUtils().getJsonFromFile(name: "venueDetails")
        XCTAssertTrue(serverResponse != nil)
        let venueDetailsModel = testInteractor.extractDetails(data: serverResponse!)
        XCTAssertTrue(venueDetailsModel != nil)
        
        XCTAssertTrue(venueDetailsModel?.bestPhoto?.prefix == "https://fastly.4sqi.net/img/general/")
        XCTAssertTrue(venueDetailsModel?.bestPhoto?.suffix == "/52070315_0EUys1-LWaAZCuB80bwethqhiJ2QH1p2iNWJ2CwHQTg.jpg")
        
        XCTAssertTrue(venueDetailsModel?.photos?[0].prefix == "https://fastly.4sqi.net/img/general/")
        XCTAssertTrue(venueDetailsModel?.photos?[0].suffix == "/52070315_0EUys1-LWaAZCuB80bwethqhiJ2QH1p2iNWJ2CwHQTg.jpg")
        
        XCTAssertTrue(venueDetailsModel?.contact?.formattedPhone == "(509) 281-3140")
        XCTAssertTrue(venueDetailsModel?.contact?.phone == "5092813140")
        
        XCTAssertTrue(venueDetailsModel?.name == "White Salmon Baking Co.")
        XCTAssertTrue(venueDetailsModel?.formattedAddress == [
            "80 Estes Ave",
            "White Salmon, WA 98672",
            "United States"
        ])
        XCTAssertTrue(venueDetailsModel?.lat == 45.7278077966147)
        XCTAssertTrue(venueDetailsModel?.lng == -121.48420916280102)
        XCTAssertTrue(venueDetailsModel?.categoryName == "Bakery")
        XCTAssertTrue(venueDetailsModel?.venueIcon?.prefix == "https://ss3.4sqi.net/img/categories_v2/food/bakery_")
        XCTAssertTrue(venueDetailsModel?.venueIcon?.suffix == ".png")
        XCTAssertTrue(venueDetailsModel?.venueUrl == nil)
        XCTAssertTrue(venueDetailsModel?.likesSummary == "14 Likes")
        XCTAssertTrue(venueDetailsModel?.isOpen == false)
        XCTAssertTrue(venueDetailsModel?.formmattedOpeningHoursToday == "None")
        XCTAssertTrue(venueDetailsModel?.tips?[0].text == "Huckleberry bread pudding or other breakfast treats")
        XCTAssertTrue(venueDetailsModel?.tips?[0].firstName == "Cusp25")

    }

}
