//
//  VenueTrackerDemoTests.swift
//  VenueTrackerDemoTests
//
//  Created by Emma Sisco on 2/11/21.
//

import XCTest

class InteractorTest: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExtractResponse() throws {
        let testInteractor = VenueListInteractor()
        let serverResponse = TestUtils().getJsonFromFile(name: "venues_search_browse")
        XCTAssertTrue(serverResponse != nil)
        let venueModel = testInteractor.extractVenueList(data: serverResponse!)
        XCTAssertTrue(venueModel != nil)
        XCTAssertTrue(venueModel!.count > 0)
        XCTAssert(venueModel![0].name == "Flywheel Sports")
        XCTAssert(venueModel![0].address == "39 W 21st St")
    }
}
