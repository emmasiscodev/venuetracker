//
//  TestUtils.swift
//  VenueTrackerDemoTests
//
//  Created by Emma Sisco on 2/12/21.
//

import Foundation
class TestUtils {
    public func getJsonFromFile(name: String) -> Data? {
        let bundle = Bundle(for: Swift.type(of: self))
        let path = bundle.path(forResource: name, ofType: "json") ?? ""
        let content = try? String(contentsOfFile: path).data(using: .utf8)
        return content
    }
}
