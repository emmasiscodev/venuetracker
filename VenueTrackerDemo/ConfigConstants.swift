//
//  ConfigConstants.swift
//  VenueTrackerDemo
//
//  Created by Emma Sisco on 2/11/21.
//

import Foundation

//In an enterprise app I would use or create a tool to obfuscate secrets from the code base and especially git hub
let CLIENT_SECRET = "0BZFG2XZIBZA53JUORHSTGH3FGLQPJ1SQMGZQNIOFDEFORD3"
let CLIENT_ID = "1AF132BBPBAELOLUOAVTASXUP2FF2UTACRPRKWTIDXFBL312"
let VENUE_SEARCH_URL = URL(string: "https://api.foursquare.com/v2/venues/search?")!
let VENU_DETAILS_URL_STRING = URL(string:"https://api.foursquare.com/v2/venues/")!
