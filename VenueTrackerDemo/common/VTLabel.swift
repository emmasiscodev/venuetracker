//
//  VTLabel.swift
//  VenueTrackerDemo
//
//  Created by Emma Sisco on 2/15/21.
//

import Foundation
import UIKit
class VTLabel: UILabel {
    
    init(title: String, fontSize: CGFloat, fontWeight: UIFont.Weight, fontColor: UIColor, containerView: UIView?){
        super.init(frame: CGRect.zero)
        text = title
        textColor = fontColor
        font = UIFont.systemFont(ofSize: fontSize, weight: fontWeight)
        translatesAutoresizingMaskIntoConstraints = false
        if let containerView = containerView {
            containerView.addSubview(self)
        }
    }
    init(title: String, fontSize: CGFloat, fontWeight: UIFont.Weight, fontColor: UIColor, stackView: UIStackView?){
        super.init(frame: CGRect.zero)
        text = title
        textColor = fontColor
        font = UIFont.systemFont(ofSize: fontSize, weight: fontWeight)
        translatesAutoresizingMaskIntoConstraints = false
        if let stackView = stackView {
            stackView.addArrangedSubview(self)
        }
    }
    init(text: NSAttributedString, fontSize: CGFloat, fontWeight: UIFont.Weight, fontColor: UIColor, stackView: UIStackView?){
        super.init(frame: CGRect.zero)
        attributedText = text
        textColor = fontColor
        font = UIFont.systemFont(ofSize: fontSize, weight: fontWeight)
        translatesAutoresizingMaskIntoConstraints = false
        if let stackView = stackView {
            stackView.addArrangedSubview(self)
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
