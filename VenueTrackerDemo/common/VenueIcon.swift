//
//  VenueIcon.swift
//  VenueTrackerDemo
//
//  Created by Emma Sisco on 2/16/21.
//

import Foundation

class VenueIcon {
    
    var prefix: String
    var suffix: String
    var size = 32
    var url: URL {
        return URL(string: "\(prefix)\(size)\(suffix)")!
    }
        
    init(iconData:[String: Any]) {
        prefix = iconData["prefix"] as! String
        suffix = iconData["suffix"] as! String
    }
}
