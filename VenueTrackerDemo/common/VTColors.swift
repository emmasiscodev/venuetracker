//
//  VTColors.swift
//  VenueTrackerDemo
//
//  Created by Emma Sisco on 2/16/21.
//

import Foundation
import UIKit

extension UIColor {
    //The color contrast wheel I used gave me colors in the range 0 - 255 but iOS wants decimal
    static var mustardBackground: UIColor { return UIColor(red: 225/255, green: 173/255, blue: 1/255, alpha: 1) }
    static var offBlackText: UIColor { return UIColor(red: 0.18, green: 0.16, blue: 0.20, alpha: 1) }
    static var topGrayGradient: UIColor { return UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 1 )}
    static var bottomGrayGradient: UIColor { return UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1 )}
}
