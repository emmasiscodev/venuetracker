//
//  extensions.swift
//  VenueTrackerDemo
//
//  Created by Emma Sisco on 2/15/21.
//
import Foundation
import UIKit

extension Double {
    func toFormattedMiles() -> String {
        //The api returns distance in meters
        //This is to convert to miles
        //As an improvement I would conditionally format this based on users preferences
        let miles = self * 0.000621
        return String(format: "%.2f miles", miles)
    }
    
    func toFormattedMeters() -> String {
        let meters = (self / 0.00062137).rounded()
        
        return String(meters)
    }
}


//https://www.hackingwithswift.com/example-code/uikit/how-to-load-a-remote-image-url-into-uiimageview
extension UIImageView {
    func loadIcon(url: URL) {
        DispatchQueue.global().async { [weak self] in
            if let data = try? Data(contentsOf: url) {
                if let image = UIImage(data: data) {
                    DispatchQueue.main.async {
                        self?.image = image.withRenderingMode(.alwaysTemplate)
                        self?.tintColor = .black
                    }
                }
            }
        }
    }
    func loadPhoto(url: URL) {
        DispatchQueue.global().async { [weak self] in
            if let data = try? Data(contentsOf: url) {
                if let image = UIImage(data: data) {
                    DispatchQueue.main.async {
                        self?.image = image
                    }
                }
            }
        }
    }
}

//I would clean this up and make it more extensible
extension URLRequest {
    func makeGetListRequest(near: String?, lat: Double?, lng: Double?) -> URLRequest {
       
        var request = URLRequest(url: url!)
        request.httpMethod = "GET"
        var components = URLComponents(url: url!, resolvingAgainstBaseURL: true)
        components?.queryItems = [
            URLQueryItem(name: "client_id", value: CLIENT_ID),
            URLQueryItem(name: "client_secret", value: CLIENT_SECRET),
            URLQueryItem(name: "v", value: "20190425"),
            URLQueryItem(name: "limit", value: "20"),
            URLQueryItem(name: "radius", value: 10.0.toFormattedMeters()),
            URLQueryItem(name: "llAcc", value: "100.0") //As an improvement I would query the devices accuracy but for demonstration purposes I assume it's accurate
        ]
        if near != nil {
            components?.queryItems?.append( URLQueryItem(name: "near", value: near))
        }
        if lat != nil && lng != nil {
            components?.queryItems?.append( URLQueryItem(name: "ll", value: "\(lat!), \(lng!)"))
        }
        request.url = components?.url
        
        return request
    }
    
    func makeGetDetailsRequest(id: String) -> URLRequest {
        var urlCopy = url!
        urlCopy.appendPathComponent(id, isDirectory: false)
        var request = URLRequest(url: urlCopy)
        var components = URLComponents(url: urlCopy, resolvingAgainstBaseURL: true)
        components?.queryItems = [
            URLQueryItem(name: "client_id", value: CLIENT_ID),
            URLQueryItem(name: "client_secret", value: CLIENT_SECRET),
            URLQueryItem(name: "v", value: "20190425")
        ]
        request.url = components?.url
        return request
    }
}
