//
//  AppDelegate.swift
//  VenueTrackerDemo
//
//  Created by Emma Sisco on 2/11/21.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        /* Create ui-view-controller instance*/
           let venue = VenueListRouter.createModule()

           let navigationController = UINavigationController()
           navigationController.viewControllers = [venue]

           window = UIWindow(frame: UIScreen.main.bounds)
           window?.rootViewController = navigationController
           window?.makeKeyAndVisible()

           return true
    }
}

