//
//  VenueListRouter.swift
//  VenueTrackerDemo
//
//  Created by Emma Sisco on 2/11/21.
//

import Foundation
import UIKit

class VenueListRouter: VenueListPresenterToRouterProtocol{
    func pushToVenueDetails(navigationController: UINavigationController, id: String) {
        let venueDetailsController = VenueDetailsRouter.createModule(id: id)
        navigationController.pushViewController(venueDetailsController, animated: true)
    }

    
    static func createModule () -> VenueListViewController {
        let view = VenueListViewController()
        let presenter: VenueListViewToPresenterProtocol & VenueListInteracterToPresenterProtocol = VenueListPresenter()
        let interactor: VenueListPresenterToInteractorProtocol = VenueListInteractor()
        let router:VenueListPresenterToRouterProtocol = VenueListRouter()

        view.presenter = presenter
        presenter.view = view
        presenter.router = router
        presenter.interactor = interactor
        interactor.presenter = presenter
        return view
    }
}
