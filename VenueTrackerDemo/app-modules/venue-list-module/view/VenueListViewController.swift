//
//  File.swift
//  VenueTrackerDemo
//
//  Created by Emma Sisco on 2/11/21.
//

import Foundation
import UIKit
import CoreLocation
class VenueListViewController: UIViewController, CLLocationManagerDelegate {
    
    
    var presenter:VenueListViewToPresenterProtocol?
    var venueTable: UITableView!
    var searchBar: UISearchBar!
    var locationIcon: UIImageView!
    var venueList: [VenueModel] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.barTintColor = .mustardBackground
        view.backgroundColor = .white
        title = "Venue Tracker"
        if #available(iOS 13.0, *) {
            locationIcon = UIImageView(image: UIImage(systemName: "location"))
        } else {
            locationIcon = UIImageView(image: UIImage(named: "Location"))
        }
        view.addSubview(locationIcon)
        locationIcon.translatesAutoresizingMaskIntoConstraints = false
        locationIcon.tintColor = .offBlackText
        locationIcon.topAnchor.constraint(equalTo: view.layoutMarginsGuide.topAnchor, constant: 8).isActive = true
        locationIcon.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -8).isActive = true
        locationIcon.widthAnchor.constraint(equalToConstant: 30).isActive = true
        locationIcon.heightAnchor.constraint(equalToConstant: 30).isActive = true
        locationIcon.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.useCurrentLocation(_:)))
        locationIcon.addGestureRecognizer(tap)

        searchBar = UISearchBar()
        view.addSubview(searchBar)
        searchBar.placeholder = "Address, City, State or Postal Code"
        searchBar.delegate = self
        searchBar.translatesAutoresizingMaskIntoConstraints = false
        searchBar.topAnchor.constraint(equalTo: view.layoutMarginsGuide.topAnchor, constant: 8).isActive = true
        searchBar.rightAnchor.constraint(equalTo: locationIcon.leftAnchor).isActive = true
        searchBar.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        searchBar.heightAnchor.constraint(equalToConstant: 30).isActive = true
      
        venueTable = UITableView()
        view.addSubview(venueTable)
        venueTable.translatesAutoresizingMaskIntoConstraints = false
        venueTable.topAnchor.constraint(equalTo: searchBar.bottomAnchor).isActive = true
        venueTable.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        venueTable.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        venueTable.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        venueTable.register(VenueCell.self, forCellReuseIdentifier: "cell")
        venueTable.delegate = self
        venueTable.dataSource = self
        presenter?.fetchVenuesWithLocation()

    }
    @objc func useCurrentLocation(_ sender: UITapGestureRecognizer? = nil) {
        searchBar.text = ""
        venueList = []
        reloadTable()
        presenter?.fetchVenuesWithLocation()
    }
    
    func reloadTable() {
        DispatchQueue.main.async {
            self.venueTable.reloadData()
        }
    }
}

extension VenueListViewController: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        presenter?.fetchVenuesWithQuery(query: searchBar.text ?? "")
    }
}

extension VenueListViewController: VenueListPresenterToViewProtocol{
    func showVenues(venues: [VenueModel]) {
        venueList = venues
        reloadTable()
    }
    
    func showError() {
        print("error")
    }
}
extension VenueListViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.venueList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! VenueCell
        cell.selectionStyle = .none
        if let name = venueList[indexPath.row].name {
            cell.name.text = name

        }
        else {
            cell.name?.isHidden = true
        }
        
        if let address = venueList[indexPath.row].address {
            cell.address.text = address
        }
        else {
            cell.address.isHidden = true
        }
        if let distance = venueList[indexPath.row].distance {
            cell.distance?.text = distance.toFormattedMiles()
        }
        else {
            cell.distance?.isHidden = true
        }
        
        if let icon = venueList[indexPath.row].icon {
            cell.icon.loadIcon(url: icon.url)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter?.showVenueDetailsController(navigationController: navigationController!, id: venueList[indexPath.row].id)
    }
}
class VenueCell: UITableViewCell {

    var icon: UIImageView!
    var name: VTLabel!
    var address: VTLabel!
    var distance: VTLabel?
    let leftRightMargins: CGFloat = 8
    let topMargin: CGFloat = 8
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        icon = UIImageView(image: UIImage(systemName: "questionmark"))
        icon.tintColor = .black
        contentView.addSubview(icon)
        icon.translatesAutoresizingMaskIntoConstraints = false
        icon.centerYAnchor.constraint(equalTo: self.contentView.centerYAnchor).isActive = true
        icon.leftAnchor.constraint(equalTo: self.contentView.leftAnchor,constant: leftRightMargins).isActive = true
        icon.heightAnchor.constraint(equalToConstant: 32).isActive = true
        icon.widthAnchor.constraint(equalToConstant: 32).isActive = true
        
        //TODO
        let stackView = UIStackView()
        contentView.addSubview(stackView)
        stackView.axis = .vertical
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.leftAnchor.constraint(equalTo: icon.rightAnchor, constant: leftRightMargins).isActive = true
        stackView.rightAnchor.constraint(equalTo: self.contentView.rightAnchor, constant: leftRightMargins).isActive = true
        stackView.topAnchor.constraint(equalTo: self.contentView.topAnchor, constant: topMargin).isActive = true
        stackView.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor, constant: -1 * topMargin).isActive = true

        name = VTLabel(title: "", fontSize: 20, fontWeight: .bold, fontColor: .offBlackText, stackView: stackView)
        address = VTLabel(title: "", fontSize: 16, fontWeight: .regular, fontColor: .black, stackView: stackView)
        distance = VTLabel(title: "", fontSize: 16, fontWeight: .regular, fontColor: .black, stackView: stackView)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
}


