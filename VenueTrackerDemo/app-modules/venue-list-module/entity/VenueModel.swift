//
//  File.swift
//  VenueTrackerDemo
//
//  Created by Emma Sisco on 2/11/21.
//

import Foundation

class VenueModel {
    var id: String
    var name: String?
    var address: String?
    var distance: Double?
    var icon: VenueIcon?
    
    init(venue: [String:Any] ){
        name = venue["name"] as? String
        id = venue["id"] as! String
        let venueLocation = venue["location"] as! [String:Any]
        address = venueLocation["address"] as? String
        distance = venueLocation["distance"] as? Double
        let venueCategories =  venue["categories"] as! [[String:Any]]
        if venueCategories.count > 0 {
            icon = VenueIcon(iconData: venueCategories[0]["icon"] as! [String: Any])
        }
    }
}


