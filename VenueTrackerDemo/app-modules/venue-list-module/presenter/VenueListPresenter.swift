//
//  VenueListPresenter.swift
//  VenueTrackerDemo
//
//  Created by Emma Sisco on 2/11/21.
//

import Foundation
import UIKit
import CoreLocation
class VenueListPresenter:NSObject, VenueListViewToPresenterProtocol {
    var locationManager = CLLocationManager()
    override init() {
        super.init()
        locationManager.delegate = self
    }
    
    func fetchVenuesWithQuery(query: String) {
        interactor?.fetchVenuesWithQuery(query: query)
    }
    
    func fetchVenuesWithLocation() {
        //Check Location and then call either interator or ask view for permissions
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
            locationManager.requestLocation()
        }
        else {
            locationManager.requestWhenInUseAuthorization()
        }
    }
    
    var view: VenueListPresenterToViewProtocol?
    var interactor: VenueListPresenterToInteractorProtocol?
    var router: VenueListPresenterToRouterProtocol?
    
    func startfetchingVenues() {
        fetchVenuesWithLocation()
    }
   
    func showVenueDetailsController(navigationController: UINavigationController, id: String) {
        router?.pushToVenueDetails(navigationController: navigationController, id: id)
    }

}

extension VenueListPresenter: VenueListInteracterToPresenterProtocol {
    func fetchVenuesSucceeded(venues: [VenueModel]) {
        view?.showVenues(venues: venues)
    }
    
    func fetchVenuesFailed() {
        view?.showError()
    }
}

extension VenueListPresenter: CLLocationManagerDelegate {
    func locationManagerDidChangeAuthorization(_ manager: CLLocationManager) {
        if CLLocationManager.authorizationStatus() == .authorizedAlways {
            manager.requestLocation()
        }
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        interactor?.fetchVenuesWithLocation(lat: locations[0].coordinate.latitude, lng: locations[0].coordinate.longitude)
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        
    }
}
