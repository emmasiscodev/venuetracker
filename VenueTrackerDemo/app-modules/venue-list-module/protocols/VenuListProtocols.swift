//
//  VenuListProtocols.swift
//  VenueTrackerDemo
//
//  Created by Emma Sisco on 2/11/21.
//

import Foundation
import UIKit

protocol VenueListViewToPresenterProtocol: class {
    var view: VenueListPresenterToViewProtocol? {get set}
    var interactor: VenueListPresenterToInteractorProtocol? {get set}
    var router: VenueListPresenterToRouterProtocol? {get set}
    
    func fetchVenuesWithQuery(query: String)
    func fetchVenuesWithLocation()
    func showVenueDetailsController(navigationController:UINavigationController, id: String)
}

protocol VenueListPresenterToViewProtocol: class {
    func showVenues(venues: [VenueModel])
    func showError()
}

protocol VenueListPresenterToRouterProtocol: class {
    static func createModule()-> VenueListViewController
    func pushToVenueDetails(navigationController:UINavigationController, id: String)
}

protocol VenueListPresenterToInteractorProtocol: class {
    var presenter: VenueListInteracterToPresenterProtocol? { get set}
    func fetchVenuesWithQuery(query: String)
    func fetchVenuesWithLocation(lat: Double, lng: Double)
}

protocol VenueListInteracterToPresenterProtocol: class {
    func fetchVenuesSucceeded(venues: [VenueModel])
    func fetchVenuesFailed()
}
