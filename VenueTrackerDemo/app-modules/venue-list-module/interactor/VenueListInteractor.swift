//
//  VenueListInteractor.swift
//  VenueTrackerDemo
//
//  Created by Emma Sisco on 2/11/21.
//

import Foundation

class VenueListInteractor: VenueListPresenterToInteractorProtocol {
    func fetchVenuesWithQuery(query: String) {
        let request = URLRequest(url: VENUE_SEARCH_URL).makeGetListRequest(near: query, lat: nil, lng: nil)
        fetchVenues(request: request)
    }
    
    func fetchVenuesWithLocation(lat: Double, lng: Double) {
        let request = URLRequest(url: VENUE_SEARCH_URL).makeGetListRequest(near: nil, lat: lat, lng: lng)
        fetchVenues(request: request)
    }
    
    var presenter: VenueListInteracterToPresenterProtocol?

    func fetchVenues(request: URLRequest) {

        URLSession.shared.dataTask(with: request) { (data, response, error) in
            guard error == nil  else {
                self.presenter?.fetchVenuesFailed()
                return
            }
            guard response != nil else {
                self.presenter?.fetchVenuesFailed()
                return
            }
            guard data != nil else {
                self.presenter?.fetchVenuesFailed()
                return
            }
            if let venues = self.extractVenueList(data: data!) {
                self.presenter?.fetchVenuesSucceeded(venues:venues)
            }
            else {
                self.presenter?.fetchVenuesFailed()
            }
           
        }.resume()
    }
    
    func extractVenueList(data: Data) -> [VenueModel]? {
        var venueList:[VenueModel] = []
        let json = try? JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String:Any]
        guard json != nil else {
            return nil
        }
        let response = json!["response"] as? [String: Any]
        guard response != nil else {
            return nil
        }
        let venues = response!["venues"] as? [[String: Any]]
        guard venues != nil else {
            return nil
        }
        venues!.forEach { (venue) in
            venueList.append(VenueModel(venue: venue))
        }
        return venueList
    }
}
