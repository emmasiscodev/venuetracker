//
//  VenueDetailsPresenter.swift
//  VenueTrackerDemo
//
//  Created by Emma Sisco on 2/13/21.
//

import Foundation
import UIKit

class VenueDetailsPresenter: VenueDetailsInteracterToPresenterProtocol {
    var router: VenueDetailsPresenterToRouterProtocol?
    var view: VenueDetailsPresenterToViewProtocol?
    var interactor: VenueDetailsPresenterToInteractorProtocol?
    var phoneNumber: String?
    var venueUrl: String?
    
    func fetchDetailsSucceeded(details: VenueDetailsModel) {
        view?.showDetails(details: details)
    }
    
    func fetchDetailsFailed() {
        view?.showError()
    }
    
}
extension VenueDetailsPresenter: VenueDetailsViewToPresenterProtocol {
    func setWebsiteUrl(website: String) {
        venueUrl = website
    }
    
    func launchWebsiteInSafari() {
        if let websiteString = venueUrl, let url = URL(string: websiteString) {
            UIApplication.shared.open(url)
        }
    }
    
    func setPhoneNumber(number: String) {
        phoneNumber = number
    }
    
    func callPhoneNumber() {
        if let number = phoneNumber, let url = URL(string: "tel://\(number)") {
             UIApplication.shared.open(url)
         }
    }
    
    func startfetchingDetails(for id: String) {
        interactor?.fetchDetails(for: id)
    }
}
