//
//  VenueDetailsModel.swift
//  VenueTrackerDemo
//
//  Created by Emma Sisco on 2/13/21.
//

import Foundation

class VenueDetailsModel {
    var id: String?
    var photos: [VenuePhoto]?
    var bestPhoto: VenuePhoto?
    var name: String?
    var contact: VenueContact?
    var formattedAddress: [String]?
    var lat: Double?
    var lng: Double? // I plan on showing this on the map given time
    var categoryName: String?
    var venueIcon: VenueIcon?
    var venueUrl: String? // I plan on open link in safari
    var likesSummary: String?
    var isOpen: Bool?
    var formmattedOpeningHoursToday: String? // I might want to show the hours for the whole week but let's start with Today
    var tips: [VenueTip]?
    
    init(venue: [String: Any]) {
        id = venue["id"] as? String
        name = venue["name"] as? String
        venueUrl = venue["url"] as? String
        if let venueContact = venue["contact"] as? [String: Any]{
            contact = VenueContact(contact: venueContact)
        }
        if let location = venue["location"] as? [String:Any]{
            formattedAddress = location["formattedAddress"] as? [String]
            lat = location["lat"] as? Double
            lng = location["lng"] as? Double
        }
        if let categories = venue["categories"] as? [[String: Any]] {
            if( categories.count > 0) {
                categories.forEach { (category) in
                    if let primary = category["primary"] as? Bool, primary == true {
                        categoryName = category["name"] as? String
                        if let iconData = category["icon"] as? [String: Any] {
                            venueIcon = VenueIcon(iconData: iconData)
                        }
                    }
                }
            }
        }
        if let likes = venue["likes"] as? [String: Any] {
            likesSummary = likes["summary"] as? String
        }
        if let photos = venue["photos"] as? [String: Any],
           let groups = photos["groups"] as? [[String:Any]],
           groups.count > 0  {
            groups.forEach { (group) in
                if let items = group["items"]  as? [[String: Any]],
                   items.count > 0{
                    items.forEach { (item) in
                        self.photos = []
                        self.photos!.append(VenuePhoto(photoData: item))
                    }
                }
            }
        }
        if let tipsData = venue["tips"] as? [String:Any],
           let groups = tipsData["groups"] as? [[String: Any]],
           groups.count > 0{
            groups.forEach { (group) in
                if let items = group["items"] as? [[String: Any]],
                   items.count > 0 {
                    items.forEach { (item) in
                        self.tips = []
                        self.tips!.append(VenueTip(tipData: item))
                    }
                }
            }
        }
        if let popular = venue["popular"] as? [String:Any] {
            isOpen = popular["isOpen"] as? Bool
            if let timeFrames = popular["timeframes"] as? [[String: Any]],
               timeFrames.count > 0{
                timeFrames.forEach { (timeFrame) in
                    if let day = timeFrame["days"] as? String,
                       day == "Today",
                       let open = timeFrame["open"] as? [[String: Any]],
                       open.count > 0{
                        formmattedOpeningHoursToday = open[0]["renderedTime"] as? String
                    }
                }
            }
            if let bestPhotoData = venue["bestPhoto"] as? [String: Any] {
                bestPhoto = VenuePhoto(photoData: bestPhotoData)
            }
        }
    }
}
class VenuePhoto {
    
    var prefix: String
    var suffix: String
    var size = "300x300"
    var url: URL {
        return URL(string: "\(prefix)\(size)\(suffix)")!
    }
        
    
    init(photoData:[String: Any]) {
        prefix = photoData["prefix"] as! String
        suffix = photoData["suffix"] as! String
    }
}
class VenueContact {
    var phone: String?
    var formattedPhone: String?
    
    init(contact: [String:Any]) {
        phone = contact["phone"] as? String
        formattedPhone = contact["formattedPhone"] as? String
    }
}
class VenueTip {
    var firstName: String?
    var lastName: String?
    var text: String?
    
    init(tipData: [String: Any]) {
        text = tipData["text"] as? String
        if let user = tipData["user"] as? [String:Any]{
            firstName = user["firstName"] as? String
            lastName = user["lastName"] as? String
        }
    }
}
