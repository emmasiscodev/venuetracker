//
//  VenueDetailProtocols.swift
//  VenueTrackerDemo
//
//  Created by Emma Sisco on 2/13/21.
//

import Foundation

import Foundation
import UIKit

protocol VenueDetailsViewToPresenterProtocol: class {
    var view: VenueDetailsPresenterToViewProtocol? {get set}
    var interactor: VenueDetailsPresenterToInteractorProtocol? {get set}
    var router: VenueDetailsPresenterToRouterProtocol? {get set}
    
    func startfetchingDetails(for id: String)
    func setPhoneNumber(number: String)
    func callPhoneNumber()
    
    func setWebsiteUrl(website: String)
    func launchWebsiteInSafari()
}

protocol VenueDetailsPresenterToViewProtocol: class {
    func showDetails(details: VenueDetailsModel)
    func showError()
    
}

protocol VenueDetailsPresenterToRouterProtocol: class {
    static func createModule(id: String)-> VenueDetailsViewController
}

protocol VenueDetailsPresenterToInteractorProtocol: class {
    var presenter: VenueDetailsInteracterToPresenterProtocol? { get set}
    func fetchDetails(for id: String)
}

protocol VenueDetailsInteracterToPresenterProtocol: class {
    func fetchDetailsSucceeded(details: VenueDetailsModel)
    func fetchDetailsFailed()
}
