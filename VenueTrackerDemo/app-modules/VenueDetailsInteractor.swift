//
//  VenueDetailsInteractor.swift
//  VenueTrackerDemo
//
//  Created by Emma Sisco on 2/13/21.
//

import Foundation

class VenueDetailsInteractor: VenueDetailsPresenterToInteractorProtocol {
    var presenter: VenueDetailsInteracterToPresenterProtocol?
    
    func fetchDetails(for id: String) {
        let request = URLRequest(url: VENU_DETAILS_URL_STRING).makeGetDetailsRequest(id: id)
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            guard error == nil  else {
                self.presenter?.fetchDetailsFailed()
                return
            }
            guard response != nil else {
                self.presenter?.fetchDetailsFailed()
                return
            }
            guard data != nil else {
                self.presenter?.fetchDetailsFailed()
                return
            }
            if let details = self.extractDetails(data: data!) {
                self.presenter?.fetchDetailsSucceeded(details: details)
            }
            else {
                self.presenter?.fetchDetailsFailed()
            }
           
        }.resume()
    }
    
    func extractDetails(data: Data) -> VenueDetailsModel? {
        let json = try? JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String:Any]
        guard json != nil else {
            return nil
        }
        let response = json!["response"] as? [String: Any]
        guard response != nil else {
            return nil
        }
        let venue = response!["venue"] as? [String: Any]
        guard venue != nil else {
            return nil
        }
        return VenueDetailsModel(venue: venue!)
    }
}
