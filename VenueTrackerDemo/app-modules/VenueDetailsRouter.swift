//
//  VenueDetailsRouter.swift
//  VenueTrackerDemo
//
//  Created by Emma Sisco on 2/13/21.
//

import Foundation
import UIKit
class VenueDetailsRouter: VenueDetailsPresenterToRouterProtocol{

    static func createModule (id: String) -> VenueDetailsViewController {
        let view = VenueDetailsViewController()
        view.id = id
        let presenter: VenueDetailsViewToPresenterProtocol & VenueDetailsInteracterToPresenterProtocol = VenueDetailsPresenter()
        let interactor: VenueDetailsPresenterToInteractorProtocol = VenueDetailsInteractor()
        let router:VenueDetailsPresenterToRouterProtocol = VenueDetailsRouter()

        view.presenter = presenter
        presenter.view = view
        presenter.router = router
        presenter.interactor = interactor
        interactor.presenter = presenter
        return view
    }
}
