//
//  VenueDetailViewController.swift
//  VenueTrackerDemo
//
//  Created by Emma Sisco on 2/13/21.
//

import Foundation
import UIKit
class VenueDetailsViewController: UIViewController {
    var id: String!
    var presenter:VenueDetailsViewToPresenterProtocol?
    var stackView: UIStackView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let scrollView = UIScrollView()
        view.addSubview(scrollView)
        view.backgroundColor = .white
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.topAnchor.constraint(equalTo: view.layoutMarginsGuide.topAnchor).isActive = true
        scrollView.leftAnchor.constraint(equalTo: view.layoutMarginsGuide.leftAnchor).isActive = true
        scrollView.rightAnchor.constraint(equalTo: view.layoutMarginsGuide.rightAnchor).isActive = true
        scrollView.bottomAnchor.constraint(equalTo: view.layoutMarginsGuide.bottomAnchor).isActive = true
        stackView = UIStackView()
        scrollView.addSubview(stackView)
        
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.topAnchor.constraint(equalTo: scrollView.topAnchor, constant: 16).isActive = true
        stackView.leftAnchor.constraint(equalTo: scrollView.leftAnchor, constant: 16).isActive = true
        stackView.rightAnchor.constraint(equalTo: scrollView.rightAnchor, constant: -16).isActive = true
        stackView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor, constant: -16).isActive = true
        stackView.alignment = .leading
        
        stackView.axis = .vertical
        presenter?.startfetchingDetails(for: id)
    }
}


extension VenueDetailsViewController: VenueDetailsPresenterToViewProtocol {
    func showDetails(details: VenueDetailsModel) {
        DispatchQueue.main.async {
            self.title = details.name
            
            if let bestPhotoData = details.bestPhoto {
                let bestPhoto = UIImageView()
                bestPhoto.loadPhoto(url: bestPhotoData.url)
                self.stackView.addArrangedSubview(bestPhoto)
                self.stackView.setCustomSpacing(20.0, after: bestPhoto)
            }
            else {
                let containerView = UIView(frame: CGRect(x: 0, y: 0, width: 300, height:300))
                let gradientLayer:CAGradientLayer = CAGradientLayer()
                gradientLayer.frame.size = containerView.frame.size
                gradientLayer.colors = [UIColor.topGrayGradient.cgColor, UIColor.bottomGrayGradient.cgColor]
                gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.0)
                gradientLayer.endPoint = CGPoint(x: 1.0, y: 1.0)
                containerView.layer.addSublayer(gradientLayer)
                
                containerView.heightAnchor.constraint(equalToConstant: 300).isActive = true
                containerView.widthAnchor.constraint(equalToConstant: 300).isActive = true

                let noPhotosAvailable = VTLabel(title: "No Photo Available", fontSize: 20, fontWeight: .light, fontColor: .black, containerView: containerView)
                noPhotosAvailable.centerYAnchor.constraint(equalTo: containerView.centerYAnchor).isActive = true
                noPhotosAvailable.centerXAnchor.constraint(equalTo: containerView.centerXAnchor).isActive = true
                
                self.stackView.addArrangedSubview(containerView)
            }
            if let likesSummary = details.likesSummary {
                let likesSummaryLabel = VTLabel(title: likesSummary, fontSize: 20, fontWeight: .light, fontColor: .black, stackView: self.stackView)
                self.stackView.setCustomSpacing(20, after: likesSummaryLabel)
            }
            
            if let categoryName = details.categoryName,
               let categoryIcon = details.venueIcon {
                let containerView = UIView()
                
                let icon = UIImageView()
                icon.translatesAutoresizingMaskIntoConstraints = false
                containerView.addSubview(icon)
                icon.loadIcon(url: categoryIcon.url)
                icon.widthAnchor.constraint(equalToConstant: 32).isActive = true
                icon.heightAnchor.constraint(equalToConstant: 32).isActive = true
                icon.leftAnchor.constraint(equalTo: containerView.leftAnchor).isActive = true
                icon.centerYAnchor.constraint(equalTo: containerView.centerYAnchor).isActive = true
    
                let category = VTLabel(title: categoryName, fontSize: 20, fontWeight: .semibold, fontColor: .black, containerView: containerView)
                category.leftAnchor.constraint(equalTo: icon.rightAnchor, constant: 16).isActive = true
                category.topAnchor.constraint(equalTo: containerView.topAnchor).isActive = true
                category.bottomAnchor.constraint(equalTo: containerView.bottomAnchor).isActive = true
                category.rightAnchor.constraint(equalTo: containerView.rightAnchor).isActive = true
                
                self.stackView.addArrangedSubview(containerView)
                self.stackView.setCustomSpacing(20, after: containerView)
            }
            
            if let address = details.formattedAddress {
                let addressStackView = UIStackView()
                addressStackView.axis = .vertical
                address.forEach { (line) in
                    _ = VTLabel(title: line, fontSize: 20, fontWeight: .regular, fontColor: .black, stackView: addressStackView)
                }
                self.stackView.addArrangedSubview(addressStackView)
                self.stackView.setCustomSpacing(5.0, after: addressStackView)

            }
            
            if let contactInfo = details.contact {
                if let phoneString =  contactInfo.formattedPhone, let rawPhoneNumber = contactInfo.phone {
                    self.presenter?.setPhoneNumber(number: rawPhoneNumber)
                    let phoneNumber = VTLabel(title: phoneString, fontSize: 20, fontWeight: .regular, fontColor: .link, stackView: self.stackView)
                    phoneNumber.isUserInteractionEnabled = true
                    phoneNumber.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.phoneNumberPressed(_:))))
                    
                    self.stackView.setCustomSpacing(5, after: phoneNumber)
                }
            }
            
            if let venueUrl = details.venueUrl {
                self.presenter?.setWebsiteUrl(website: venueUrl)
                let urlLabel = VTLabel(title: venueUrl, fontSize: 20, fontWeight: .light, fontColor: .blue, stackView: self.stackView)
                urlLabel.isUserInteractionEnabled = true
                
                urlLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.websitePressed(_:))))
                self.stackView.setCustomSpacing(20, after: urlLabel)
            }
            
            if let isOpenToday = details.isOpen {
                let openTodayLabel: VTLabel!
                if isOpenToday {
                    openTodayLabel = VTLabel(title: "Open Now", fontSize: 20, fontWeight: .regular, fontColor: .black, stackView: self.stackView)
                }
                else {
                    openTodayLabel = VTLabel(title: "Closed", fontSize: 20, fontWeight: .regular, fontColor: .black, stackView: self.stackView)
                }
                self.stackView.setCustomSpacing(5, after: openTodayLabel)
            }
            
            if let todaysHours = details.formmattedOpeningHoursToday {
                
                let attributedString = NSMutableAttributedString(string: "Todays Hours: \(todaysHours)")
                attributedString.addAttribute(.font, value: UIFont.systemFont(ofSize: 20), range: NSRange(location: 0, length: 12))
                attributedString.addAttribute(.font, value: UIFont.systemFont(ofSize: 16), range: NSRange(location: 12, length: todaysHours.count))
                let todaysHoursLabel = VTLabel(text: attributedString, fontSize: 20, fontWeight: .regular, fontColor: .black, stackView: self.stackView)
                
                self.stackView.setCustomSpacing(20, after: todaysHoursLabel)
            }
            if let tips = details.tips {
                let tipStackView = UIStackView()
                tipStackView.axis = .vertical
                tipStackView.alignment = .leading
                _ = VTLabel(title: "Tips: ", fontSize: 20, fontWeight: .regular, fontColor: .black, stackView: tipStackView)
                tips.forEach { (tip) in
                    let containerView = UIView()
                    containerView.widthAnchor.constraint(equalToConstant: 300).isActive = true
                    
                    let firstName = tip.firstName ?? ""
                    let lastname = tip.lastName ?? ""
                    let text = tip.text ?? ""
                    let tipText = "\(firstName) \(lastname) : \(text)"
                    let tipLabel = VTLabel(title: tipText, fontSize: 16, fontWeight: .light, fontColor: .black, containerView: containerView)
                    tipLabel.topAnchor.constraint(equalTo: containerView.topAnchor).isActive = true
                    tipLabel.rightAnchor.constraint(equalTo: containerView.rightAnchor).isActive = true
                    tipLabel.leftAnchor.constraint(equalTo: containerView.leftAnchor).isActive = true
                    tipLabel.bottomAnchor.constraint(equalTo: containerView.bottomAnchor).isActive = true
                    tipLabel.numberOfLines = 0
                    tipLabel.lineBreakMode = .byWordWrapping
                    tipStackView.addArrangedSubview(containerView)
                }
                self.stackView.addArrangedSubview(tipStackView)
            }
        }
    }
    
    @objc func phoneNumberPressed(_ sender: UITapGestureRecognizer) {
        presenter?.callPhoneNumber()
    }
    @objc func websitePressed(_ sender: UITapGestureRecognizer) {
        presenter?.launchWebsiteInSafari()
    }
    
    func showError() {
        DispatchQueue.main.async {
            // I would want to be more descriptive and show real reasoning for why the error to cover various error cases.
            let alert = UIAlertController(title: "I'm sorry we could not get the data", message: "We might have hit the rate limit", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Dismiss", style: .default, handler: nil))
            self.present(alert, animated: true)
        }
    }
}
