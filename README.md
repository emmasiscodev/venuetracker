# README #

### What is this repository for? ###

This is a sample project designed using VIPER architecture and protocol driven communication.

### Design Considerations ###

For the Entitites I decided to use raw JSONSerialization to walk through the Json structure as a dictionary and unpack just a few specific variables that I wanted.
This approach worked well given that FourSquare Places API responses have a lot of data that is not needed. Also given that the database is user driven,
there are many optional values.

I initially considered using Codables and a JSONDecoder to turn the JSON into Swift Objects.
Upon start it became apparent that I did not need much of the data and it would be easier to walk through and select specific values then model the whole response.


For the Interactors I decided that I would have those classes make the API call directly.
If there was a requirement that the data be stored in a local database for caching or offline purposes,
then I would have had the Interactor decided whether it was going to call out to a seperate NetworkService class to make the network call or
to a Repository like Core Data.

For the VenueDetailViewController I decided to conditionally add views to a stack view when I get back the details of the venue.
The stack view maximizes showing whatever data is available as well as to not have to manage variable top and bottom constraints depending on what views are showing.
If the data is available then I add it to the stack view, otherwise I do not.

### How do I get set up? ###

This app requires a device running iOS 13 or greater.
